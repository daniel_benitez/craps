#Crear un objeto de la clase Dado

from dado import Dado
from jugador import Jugador
from mesa import Mesa


dado_ejemplo = Dado()
dado_ejemplo2 = Dado()

jugador_ejemplo = Jugador()
jugador_ejemplo.lanzar(dado_ejemplo,dado_ejemplo2)

mesa_ejemplo = Mesa
mesa_ejemplo.iniciar_juego(jugador_ejemplo)

pass